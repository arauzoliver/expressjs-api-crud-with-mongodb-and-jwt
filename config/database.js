//Set up mongoose connection
const mongoose = require('mongoose');
const mongoDB = `mongodb+srv://${process.env.DBUSER}:${process.env.DBPASS}@cluster0.qgkjhpt.mongodb.net/${process.env.DBNAME}`;

mongoose.connect(mongoDB)
.then(() => console.log('mongoDB connected...'))
.catch(e => console.log(e));

mongoose.Promise = global.Promise;

module.exports = mongoose;