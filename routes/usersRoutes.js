const express = require('express');
const {body} = require('express-validator')
const router = express.Router();
const userController = require('../api/controllers/usersController.js');

router.post('/register', [
    body("email", 'invalid Email!').trim().isEmail().normalizeEmail(),
    body("name", 'invalid name!').trim(),
    body("password", 'invalid password!').trim().isLength({min:6}).custom((value, {req}) => {
        if(value !== req.body.repassword) {
            throw new Error('passwords do not match');
        }
        return value;
    })], 
    userController.create);

router.post('/authenticate', [
    body("email", 'invalid Email!').trim().isEmail().normalizeEmail(),
    body("password", 'invalid password!').trim().isLength({min:6})],
    userController.authenticate);

module.exports = router;