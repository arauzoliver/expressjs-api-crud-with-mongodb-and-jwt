const express = require('express');
const router = express.Router();
const purchaseController = require('../api/controllers/purchaseController.js');

router.get('/:userId', purchaseController.getAll);
router.post('/', purchaseController.buy);
//router.get('/:productId', purchaseController.getById);

module.exports = router;