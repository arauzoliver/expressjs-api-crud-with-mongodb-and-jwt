const jwt = require('jsonwebtoken');

export const getToken = (uid) => {
    const expiresIn = 60*15;
    try {
        const token = jwt.sign({uid}, process.env.SECRET, { expiresIn});
        return { token, expiresIn };
    } catch (error) {
        console.log(error)
    }
}