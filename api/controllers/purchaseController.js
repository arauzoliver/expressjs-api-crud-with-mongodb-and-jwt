const purchaseModel = require('../models/purchase.js');

module.exports = {
    buy: async function(req, res, next) {
        try {
            await purchaseModel.create({ userId: req.body.userId, productId: req.body.productId });
            res.json({status: "success", message: "Product purchased successfully!"});
        } catch (error) {
            return res.status(500).json({error: "Request error"});
        }
    },

    getAll: async function(req, res, next) {
        try {
            const purchasedProduct = await purchaseModel.find({userId: req.params.userId});
            if(!purchasedProduct) return res.status(403).json({ error: 'Products doesn´t exist' });
            return res.json({status:"Success", message: "Products found", data:{products: purchasedProduct}});
        } catch (error) {
            return res.status(404).json({error: "Request error"});
        }
    }
}