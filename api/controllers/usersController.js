const userModel = require('../models/users');
const bcrypt = require('bcrypt'); 
const jwt = require('jsonwebtoken');

module.exports = {
    create: async function(req, res, next) {

        const { name, email, password } = req.body;
        
        try {
            const user = new userModel({name, email, password});
            await user.save();
            return res.json({msg: 'successfully registered!'});
        } catch (error) {
            if (error.keyPattern.email >0 ) {
                return res.status(400).json({error: "Email already exists"});
            }
            return res.status(500).json({error: "Server error"})
        }
    },

    authenticate: async function(req, res, next) {

        try {
            const { name, email, password } = req.body;
            let user = await userModel.findOne({ email });
            if(!user) return res.status(403).json({ error: 'User doesn´t exist' });
            
            const passwordValidation = await bcrypt.compareSync(req.body.password, user.password)
            if(!passwordValidation) {
                return res.status(403).json({ error: 'Wrong credentials' });
            }

            const token = jwt.sign({id: user.id}, process.env.SECRET, { expiresIn: '1h' });
            return res.json({status:"Success", message: "User found!", data:{user: user.id, token: token}});

        } catch (error) {
            return res.status(500).json({ error: 'Server error' });
        }
    },
}