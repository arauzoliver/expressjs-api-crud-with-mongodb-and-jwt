const productModel = require('../models/products.js');

module.exports = {
    getById: async function(req, res, next) {
        try {
            const product = await productModel.findById(req.params.productId);
            res.json({status: "success", message: "Product found", data: product});
        } catch (error) {
            return res.status(404).json({error: "Request error"});
        }
    },

    getAll: async function(req, res, next) {
        try {
            const products = await productModel.find({});
            if(!products) return res.status(403).json({ error: 'Products doesn´t exist' });
            return res.json({status:"Success", message: "Products found", data:{products: products}});
        } catch (error) {
            return res.status(404).json({error: "Request error"});
        }
    },

    updateById: async function(req, res, next) {
        try {
            await productModel.findByIdAndUpdate(req.params.productId,{ name: req.body.name, details: req.body.details })
            const editedProduct = await productModel.findById(req.params.productId);
            res.json({status: "success", message: "Product edited", data: editedProduct});
        } catch (error) {
            return res.status(404).json({error: "Request error"});
        }
    },

    deleteById: async function(req, res, next) {
        try {
            await productModel.findByIdAndRemove(req.params.productId)
            res.json({status:"success", message: "Product deleted successfully!"});
        } catch (error) {
            return res.status(404).json({error: "Request error"});
        }
    },

    create: function(req, res, next) {
        try {
            productModel.create({ name: req.body.name, details: req.body.details });
            res.json({status: "success", message: "Product added successfully!", data: null});
        } catch (error) {
            return res.status(500).json({error: "Request error"});
        }
    },
}