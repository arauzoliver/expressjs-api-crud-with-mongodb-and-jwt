const mongoose = require('mongoose');
//Define a schema
const Schema = mongoose.Schema;
const PurchaseSchema = new Schema({
 userId: {
  type: String,
  trim: true,  
  required: true,
 },
 productId: {
  type: String,
  trim: true,
  required: true
 }
});
module.exports = mongoose.model('Purchase', PurchaseSchema)